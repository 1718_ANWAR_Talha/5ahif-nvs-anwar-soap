package miniwebservice;


import java.sql.*;
import java.util.TimeZone;

public class MySQLConn {
    Connection conn = null;

    public MySQLConn() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/soap?serverTimezone=" + TimeZone.getDefault().getID(), "root", "root");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet retrieveData(int count) throws SQLException {
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("select * from p_person order by p_personid limit "+count);
        conn.close();
        return rs;
    }
}