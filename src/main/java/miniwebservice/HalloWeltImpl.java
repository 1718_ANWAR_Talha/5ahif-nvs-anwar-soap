package miniwebservice;

import javax.jws.WebService;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

@WebService( endpointInterface = "miniwebservice.HalloWelt")
public class HalloWeltImpl implements HalloWelt{
    public String hallo (String wer) { return "Hallo " + wer; }

    @Override
    public Object rechner(String rechnung) {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("js");
        try {
            Object result = engine.eval(rechnung);
            return rechnung + "=" + result.toString();
        } catch(Exception e) {
            return "Fehler, bitte syntaktisch korrekte Rechnung, wie z.B. 3+3, eingeben!";
        }
    }

    public static boolean isNumeric(String strNum) {
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }
}