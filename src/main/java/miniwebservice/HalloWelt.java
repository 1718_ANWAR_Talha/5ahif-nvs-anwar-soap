package miniwebservice;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.ResultSet;

@WebService
public interface HalloWelt {
    public String hallo(@WebParam(name = "wer") String wer);

    public Object rechner(@WebParam(name = "rechnung") String rechnung);
}
