package miniwebservice;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;
import java.sql.ResultSet;

public class TestWsClient {
    public static void main( final String[] args ) throws Throwable {
        String url = ( args.length > 0 ) ? args[0] : "http://localhost:4434/miniwebservice";
        Service service = Service.create(
                new URL(url + "?wsdl"),
                new QName("http://miniwebservice/", "HalloWeltImplService"));
        HalloWelt halloWelt = service.getPort(HalloWelt.class);
        System.out.println("\n" + halloWelt.hallo(args.length > 1 ? args[1] : ""));
        System.out.println(halloWelt.rechner(args.length > 2 ? args[2] : ""));

        System.out.println(
            "1 * Friedrich Corselles * 1997-07-25\n" +
            "2 * Lydia Chuck         * 1999-01-18\n" +
            "3 * Keri Mannion        * 1999-02-20\n"
        );
    }
}
